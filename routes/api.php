<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Faqcategories
    Route::apiResource('faq-categories', 'FaqCategoryApiController');

    // Faqquestions
    Route::apiResource('faq-questions', 'FaqQuestionApiController');
    
    // Contentcategories
    Route::apiResource('content-categories', 'ContentCategoryApiController');

    // Contenttags
    Route::apiResource('content-tags', 'ContentTagApiController');

    // Contentpages
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::apiResource('content-pages', 'ContentPageApiController');

    // Assetcategories
    Route::apiResource('asset-categories', 'AssetCategoryApiController');

    // Assetlocations
    Route::apiResource('asset-locations', 'AssetLocationApiController');

    // Assetstatuses
    Route::apiResource('asset-statuses', 'AssetStatusApiController');

    // Assets
    Route::post('assets/media', 'AssetApiController@storeMedia')->name('assets.storeMedia');
    Route::apiResource('assets', 'AssetApiController');
});
