<?php

namespace App\Http\Controllers;
use App\Asset;

use Illuminate\Http\Request;

class UserHomeController extends Controller
{
    /**
     * Show the application home page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $assets = Asset::all();
        return view('user.index', compact('assets'));
    }
}
