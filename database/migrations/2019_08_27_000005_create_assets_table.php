<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->date('date')->nullable();
            $table->string('budget')->nullable();
            $table->string('nbAssociers')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
