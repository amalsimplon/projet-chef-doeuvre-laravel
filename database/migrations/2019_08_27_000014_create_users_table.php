<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->boolean('verified')->default(0)->nullable();
            $table->datetime('verified_at')->nullable();
            $table->string('verification_token')->nullable();
            $table->longText('bio')->nullable();
            $table->string('phone')->nullable();
            $table->string('adress')->nullable();
            $table->unsignedInteger('expertise_id');
            $table->foreign('expertise_id')->references('id')->on('asset_categories');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
