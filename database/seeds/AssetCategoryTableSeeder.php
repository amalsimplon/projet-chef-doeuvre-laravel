<?php

use App\AssetCategory;
use Illuminate\Database\Seeder;

class AssetCategoryTableSeeder extends Seeder
{
    public function run()
    {
        $assetCategories = [[
            'id'         => '1',
            'name'       => 'IT',
            'created_at' => '2019-08-27 09:41:04',
            'updated_at' => '2019-08-27 09:41:04',
        ],
            [
                'id'         => '2',
                'name'       => 'Agronomie',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '3',
                'name'       => 'Environnement',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '4',
                'name'       => 'Robotics',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '5',
                'name'       => 'Agroalimentaire',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '6',
                'name'       => 'Banque / Assurance',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '7',
                'name'       => 'Chimie / Parachimie',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '8',
                'name'       => 'Électronique / Électricité',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '9',
                'name'       => 'Industrie pharmaceutique',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '10',
                'name'       => 'Transports / Logistique',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '11',
                'name'       => 'Textile / Habillement / Chaussure',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '12',
                'name'       => 'Plastique / Caoutchouc',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '13',
                'name'       => 'Bois / Papier / Carton / Imprimerie',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ]];

        AssetCategory::insert($assetCategories);
    }
}
