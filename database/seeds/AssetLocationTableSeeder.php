<?php

use App\AssetLocation;
use Illuminate\Database\Seeder;

class AssetLocationTableSeeder extends Seeder
{
    public function run()
    {
        $assetLocations = [[
            'id'         => '1',
            'name'       => 'Ariana',
            'created_at' => '2019-08-27 09:41:04',
            'updated_at' => '2019-08-27 09:41:04',
        ],
            [
                'id'         => '2',
                'name'       => 'Beja',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '3',
                'name'       => 'Ben arous',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '4',
                'name'       => 'Bizerte',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '5',
                'name'       => 'Gabes',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '6',
                'name'       => 'Gafsa',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '7',
                'name'       => 'Jendouba',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '8',
                'name'       => 'Kairouan',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '9',
                'name'       => 'kasserine',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '10',
                'name'       => 'Kebili',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '11',
                'name'       => 'Kef',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '12',
                'name'       => 'Mahdia',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '13',
                'name'       => 'Manouba',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '14',
                'name'       => 'Medenin',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '15',
                'name'       => 'Monastir',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '16',
                'name'       => 'Nabeul',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '17',
                'name'       => 'Sfax',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '18',
                'name'       => 'SidiBouzid',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '19',
                'name'       => 'Siliana',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '20',
                'name'       => 'Sousse',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '21',
                'name'       => 'Tataouine',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '22',
                'name'       => 'Tozeur',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '23',
                'name'       => 'Tunis',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ],
            [
                'id'         => '24',
                'name'       => 'Zaghouan',
                'created_at' => '2019-08-27 09:41:04',
                'updated_at' => '2019-08-27 09:41:04',
            ]];

        AssetLocation::insert($assetLocations);
    }
}
