<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [[
            'id'                 => 1,
            'name'               => 'Admin',
            'email'              => 'admin@admin.com',
            'password'           => '$2y$10$V/JeirlrKBTLORiRf0wBmemh343hZMGFuBG2Asjd1ay5ATWE1bh4m',
            'remember_token'     => null,
            'created_at'         => '2019-08-21 14:40:26',
            'updated_at'         => '2019-08-21 14:40:26',
            'deleted_at'         => null,
            'verified'           => 1,
            'verified_at'        => '2019-08-21 14:40:26',
            'verification_token' => '',
            'bio'                => 'bla bla',
            'phone'              => '20000000',
            'adress'             => 'patata patati',
            'expertise_id'       => 6,
            
        ]];

        User::insert($users);
    }
}
