@extends('layouts.app')
@section('content')
<div class="content">
  <div class="panel">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <h2>New here?</h2>
          <h4 style="padding:20px">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.</p>
        </div>
        <div class="col-md-6">
          <img src="images/pablo-downloading.png" alt="">
        </div>
      </div>
      <br>
      @foreach($faqQuestions as $key => $faqQuestion)
      <div class="panel-group">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" href="#{{ $faqQuestion->id}}">
                  {{ $faqQuestion->question ?? '' }}</a>
            </h4>
          </div>
          <div id="{{ $faqQuestion->id}}" class="panel-collapse collapse">
            <div class="panel-body">{{ $faqQuestion->answer ?? '' }}</div>
          </div>
        </div>
      </div>
@endforeach

        </div>
      </div>
</div>
@endsection
@section('scripts')
@parent

@endsection