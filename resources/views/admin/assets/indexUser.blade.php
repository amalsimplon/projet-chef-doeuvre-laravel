@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="panel">
    <div class="panel-body">
        <div class="row">
            @foreach($assets as $key => $asset)
                <div class="col-md-4 col-sm">
                        <h4>{{ $asset->name ?? '' }}</h4>
                        <p>{{ $asset->description ?? '' }}</p>
                        <p>{{ $asset->assigned_to->name ?? '' }}</p>
                </div>
            @endforeach
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
@parent

@endsection