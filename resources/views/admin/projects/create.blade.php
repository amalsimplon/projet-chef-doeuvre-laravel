@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.project.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.projects.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                            <label for="category">{{ trans('cruds.project.fields.category') }}*</label>
                            <input type="text" id="category" name="category" class="form-control" value="{{ old('category', isset($project) ? $project->category : '') }}" required>
                            @if($errors->has('category'))
                                <p class="help-block">
                                    {{ $errors->first('category') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.project.fields.category_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection