@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.faqCategory.title_singular') }}
                </div>
                <div class="panel-body">

                    <form action="{{ route("admin.faq-categories.update", [$faqCategory->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                            <label for="category">{{ trans('cruds.faqCategory.fields.category') }}*</label>
                            <input type="text" id="category" name="category" class="form-control" value="{{ old('category', isset($faqCategory) ? $faqCategory->category : '') }}" required>
                            @if($errors->has('category'))
                                <p class="help-block">
                                    {{ $errors->first('category') }}
                                </p>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.faqCategory.fields.category_helper') }}
                            </p>
                        </div>
                        <div>
                            <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection