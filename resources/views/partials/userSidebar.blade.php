<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <ul class="sidebar-menu tree" data-widget="tree">
            <li>
                <a href="">
                    <i class="fas fa-fw fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            <li>
                <a href="{{ route("faq-questions") }}">
                        <i class="fa-fw fas fa-question">
                        </i>
                        <span>{{ trans('cruds.faqQuestion.title') }}</span>
                </a>
            </li>
            


        </ul>
    </section>
</aside>