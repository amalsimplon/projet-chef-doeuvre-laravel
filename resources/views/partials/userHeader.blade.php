<a href="/" class="logo">
    <span class="logo-mini"><b>P.ChO</b></span>
    <span class="logo-lg">{{ trans('panel.site_title') }}</span>
</a>

<nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">{{ trans('global.toggleNavigation') }}</span>
    </a>

    @if(count(config('panel.available_languages', [])) > 1)
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{ strtoupper(app()->getLocale()) }}
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <ul class="menu">
                                @foreach(config('panel.available_languages') as $langLocale => $langName)
                                    <li>
                                        <a href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a>
                                    </li>
                                @endforeach
                                
                            </ul>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
    @endif
    <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <li class="navbar-brand"></li>
        <li class="navbar-brand">
            Get started
        </li>
        <a href="{{ route('register') }}">
            <li class="navbar-brand"> sign up</li>
        </a>
        <a href="{{ route('login') }}">
                <li class="navbar-brand"> login</li>
        </a>
              
    </ul>
    </div>
</nav>