<!DOCTYPE html>
<html>

<head>
    @include('partials.userHead')
</head>
<body class="sidebar-mini" style="height: auto; min-height: 100%;">
    <div class="wrapper" style="height: auto; min-height: 100%;">
        <header class="main-header">
                <a href="/" class="logo">
                    <span class="logo-mini"><b>{{ trans('panel.site_title') }}</b></span>
                    <span class="logo-lg">{{ trans('panel.site_title') }}</span>
                </a>                
                <nav class="navbar navbar-static-top registerNav">
                    <ul class="nav navbar-nav ">
                        <a href="/"><li class="navbar-brand">
                                    Home
                        </li>   </a> 
                        <li class="navbar-brand">
                                    Get started 
                        </li>
                    </ul>
                </nav>       
        </header>
    <div class="container registerBox">
                    @if(session('message'))
                    <div class="row" style='padding:20px 20px 0 20px;'>
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">{{ session('message') }}</div>
                        </div>
                    </div>
                @endif
                @if($errors->count() > 0)
                    <div class="row" style='padding:20px 20px 0 20px;'>
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                @yield('content')
            
        
    </div>
        <footer class="footer text-center">
            <strong>{{ trans('panel.site_title') }} &copy;</strong> {{ trans('global.allRightsReserved') }}
        </footer>

        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>

@include('partials.scripts')
</body>

</html>